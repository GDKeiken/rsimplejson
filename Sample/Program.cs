﻿using System;
using System.Collections.Generic;

using RSimpleJson;
using RSimpleJson.Objects;


namespace Sample
{
	#region Helper
	//public static class Json
	//{
	//	#region Public Methods
	//	public static object Deserialize(string json)
	//	{
	//		return JSON.Deserialize(json);
	//	}

	//	//public static T Deserialize<T>(string json) where T : new()
	//	//{
	//	//	if (typeof(T) != typeof(JsonObject) && typeof(T) != typeof(JsonArray))
	//	//	{
	//	//		return JSON.Deserialize<T>(json);
	//	//	}
	//	//	else
	//	//	{
	//	//		return (T)JSON.Deserialize(json);
	//	//	}
	//	//}

	//	public static T DeserializeObject<T>(object jsonObject, string rootElement = null) where T : new()
	//	{
	//		return JSON.DeserializeObject<T>(jsonObject, rootElement);
	//	}

	//	public static string Serialize(object obj)
	//	{
	//		string str = JSON.Serialize(obj);

	//		if (string.IsNullOrEmpty(str))
	//		{
	//			// TODO: Error
	//		}

	//		return str;
	//	}
	//	#endregion
	//}
	#endregion

	#region Json Serializer
	public class SampleJsonSerializer : JsonSerializer
	{
		protected override bool TrySerializeCustomType(object input, out object output)
		{
			bool result = true;
			if (input is ExampleStruct)
			{
				ExampleStruct val = (ExampleStruct)input;
                IDictionary<string, object> obj = new Dictionary<string, object>();
				obj.Add("IntValue", val.IntValue);
				obj.Add("FloatValue", val.FloatValue);
				obj.Add("DoubleValue", val.DoubleValue);
				obj.Add("StringValue", val.StringValue);
				output = obj;
			}
			else
			{
				result = false;
				output = null;
			}

			return result;
        }

		protected override bool TryDeserializeCustomType(Type type, IDictionary<string, object> input, out object output)
		{
			bool result = true;
			if (type == typeof(ExampleStruct))
			{
				output = new ExampleStruct(Convert.ToInt32(input["IntValue"]), Convert.ToSingle(input["FloatValue"]), Convert.ToDouble(input["DoubleValue"]), (string)input["StringValue"]);
			}
			else
			{
				result = false;
				output = null;
			}

			return result;
		}
	}
	#endregion

	[JsonSerializable] // SerializableAttribute can also be used
	struct ExampleStruct
	{
		public int IntValue;
		public float FloatValue;
		public double DoubleValue;
		public string StringValue;

		public ExampleStruct(int intValue = 0, float floatValue = 0.0f, double doubleValue = 0.0, string stringValue = null)
		{
			IntValue = intValue;
			FloatValue = floatValue;
			DoubleValue = doubleValue;
			StringValue = stringValue;
        }
	}

	[JsonSerializable]
	class Base
	{
		// NOTE: All public values will be serialized, use the attribute [NonSerialized] to excute or [JsonInclude] to include a field
		// Properties will always need to be included

		public string helloString = "";
		
		public string HelloString { get { return helloString; } set { helloString = value;  } }
	}

	[JsonSerializable]
	class Test
	{
		public string test;
	}

	[JsonSerializable]
	class ChildOne : Base, IJsonSerializable
	{
		public Test AmIAClass = null;
		public ExampleStruct testStruct;
		public object AppendTo(IDictionary<string, object> dictionary)
		{
			dictionary.Add("@hello", null);

			return this;
		}
	}

	[JsonSerializable]
	class ChildTwo : Base
	{
		public string stringTest = null;
	}

	class Program
	{
		static void Main(string[] args)
		{
			// Allows the ExampleStruct to be serialized correctly
			JSON.CurrentJsonSerializer = new SampleJsonSerializer();
			JSON.CurrentJsonSerializer.SetOptions(EncodeOptions.All);

			ChildOne child1 = new ChildOne();
			child1.testStruct = new ExampleStruct(-99, 2.0f, 3.4, "weeeeeee");
			child1.helloString = "Hello world";
			child1.AmIAClass = new Test();
			child1.AmIAClass.test = "maybe";

			string json = JSON.Serialize(child1);
			Console.WriteLine(json);
			object jk = JSON.Deserialize(json);
			object ttt = JSON.DeserializeStringAsObject<ChildOne>(json);

			List<int> fghjdk = new List<int>();
			fghjdk.Add(54);
			fghjdk.Add(55);
			fghjdk.Add(545);
			fghjdk.Add(546457);
			json = JSON.Serialize(fghjdk);
			Console.WriteLine(json);
			jk = JSON.Deserialize(json);
			string uty = jk.GetType().ToString();

			StructSerialization();
			ClassSerialization();

			Console.ReadLine();
		}

		static void StructSerialization()
		{
			Console.WriteLine("\n == Struct Serialization ==\n");

			ExampleStruct structTest = new ExampleStruct();
			structTest.IntValue = 9;
			string json = JSON.Serialize(structTest);
			Console.WriteLine(json);

			ExampleStruct structTestResult;
			try
			{
				structTestResult = JSON.DeserializeStringAsObject<ExampleStruct>(json);
			}
			catch
			{
				Console.WriteLine("\nERROR: Struct contains no default constructor\n");
				// This will fail unless you extend the JsonSerializer to handle the serialization of this object
				// Since structs are not allowed a default constructor
			}
		}

		static void ClassSerialization()
		{
			Console.WriteLine("\n == Class Serialization == \n");

			List<object> list = new List<object>();
			Dictionary<string, object> dict = new Dictionary<string, object>();
			ChildOne child1 = new ChildOne();
			child1.testStruct = new ExampleStruct(-99, 2.0f, 3.4, "weeeeeee");
			child1.helloString = "Hello world";
			list.Add(child1);
			dict.Add("child1", child1);
			object child2 = new ChildTwo();
			((ChildTwo)child2).stringTest = "I won't be set on deserialized?!?!?";
			((ChildTwo)child2).helloString = "Goodbye";
			list.Add(child2);
			dict.Add("child2", child2);

			string json = JSON.Serialize(dict);
			Console.WriteLine(json);

			object sdfdict = JSON.DeserializeStringAsObject<Dictionary<string, object>>(json);

			json = JSON.Serialize(list);
			Console.WriteLine(json);

			object sdfd = JSON.DeserializeStringAsObject<List<object>>(json);
			return;

			/*List<Base> listResult = Json.Deserialize<List<Base>>(json);
			ChildOne child1Result = (ChildOne)listResult[0];
			try
			{
				ChildTwo child2Result = (ChildTwo)listResult[1];
			}
			catch
			{
				Console.WriteLine("\nERROR: List result at index 1 is '{0}' cannot case as '{1}'\n", listResult[1].GetType(), typeof(ChildTwo));
				// This will fail because it is a Base object not a ChildTwo object after the deserialization
			}*/
		}
	}
}
